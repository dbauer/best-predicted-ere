import sys
from deft_ere import read_ere_xml
from deft_best import read_best_xml

from collections import defaultdict

import xml.dom.minidom

import itertools
from xml.etree.ElementTree import tostring

import os.path
import codecs

def get_entity_mention_dict(ere):
    mentions = {} 
    for mention_id in ere.entity_mentions:
        mention = ere.entity_mentions[mention_id]
        if mention.nom_head:
            head_offset, head_length = mention.nom_head.offset, mention.nom_head.length
        else: 
            head_offset, head_length = mention.offset, mention.length
        #signature = (mention.offset, mention.length, head_offset, head_length, mention.entity.entity_type)
        signature = (head_offset, head_length, mention.entity.entity_type)

        if signature in mentions:
            sys.stderr.write("WARNING, two entity mention annotations for this span. "+ str(signature)+"\n")
        mentions[signature] = mention
    return mentions


def compute_entity_mention_name_map(gold_ere, predicted_ere):
    i = 1

    gold_mentions = get_entity_mention_dict(gold_ere)
    predict_mentions = get_entity_mention_dict(predicted_ere)
    
    span_to_new_id = {}
    all_mention_spans = set(gold_mentions.keys()).union(predict_mentions.keys())
    for mention_span in all_mention_spans:
        span_to_new_id[mention_span] = "m-"+str(i)
        i += 1

    gold_mentions_to_new = {}
    for mention_span in gold_mentions:
        mention = gold_mentions[mention_span]
        gold_mentions_to_new[mention.mention_id] = span_to_new_id[mention_span]

    predict_mentions_to_new = {}
    for mention_span in predict_mentions:
        mention = predict_mentions[mention_span]
        predict_mentions_to_new[mention.mention_id] = span_to_new_id[mention_span]

    return gold_mentions_to_new, predict_mentions_to_new


def get_relation_mention_dict(ere):
    mentions = {} 
    for mention_id in ere.relation_mentions:
        mention = ere.relation_mentions[mention_id]

        arg1 = mention.rel_arg1.entity_mention.mention_id if mention.rel_arg1.entity_mention else None
        arg2 = mention.rel_arg2.entity_mention.mention_id if mention.rel_arg2.entity_mention else None

        rel_type = mention.relation.relation_type

        signature = (arg1, arg2, rel_type)

        if signature in mentions:
            sys.stderr.write("WARNING, two annotations for this relation ("+ str(arg1) +" "+str(arg2)+"). Only recording one potential match.\n") 
        mentions[signature] = mention
    return mentions

def compute_relation_mention_name_map(gold_ere, predicted_ere):
    i = 1

    gold_mentions = get_relation_mention_dict(gold_ere)

    predict_mentions = get_relation_mention_dict(predicted_ere)
  
    span_to_new_id = {}
    all_mention_spans = set(gold_mentions.keys()).union(predict_mentions.keys())
    for mention_span in all_mention_spans:
        span_to_new_id[mention_span] = "relm-"+str(i)
        i += 1

    gold_mentions_to_new = {}
    for mention_span in gold_mentions:
        mention = gold_mentions[mention_span]
        gold_mentions_to_new[mention.mention_id] = span_to_new_id[mention_span]

    predict_mentions_to_new = {}
    for mention_span in predict_mentions:
        mention = predict_mentions[mention_span]
        predict_mentions_to_new[mention.mention_id] = span_to_new_id[mention_span]

    return gold_mentions_to_new, predict_mentions_to_new


def get_event_mention_dict(ere):
    mentions = defaultdict(list)
    for mention_id in ere.event_mentions:
        mention = ere.event_mentions[mention_id]

        event_type = mention.event_type.lower()

        trigger = mention.trigger
        offset = trigger.offset
        length = trigger.length

        signature = (offset, length, event_type)

        if signature in mentions:
            sys.stderr.write("WARNING, two annotations for this event trigger and type ("+str(offset)+","+str(length)+","+event_type+")\n") 
        mentions[signature].append(mention)
    return mentions


def get_arg_overlap_map(gold_ems, predict_ems):
    """
    map event mentions to each other based on overlap of their arguments
    """

    gold_args_for_em = [(em.mention_id, set(em.argument_index.keys())) for em in gold_ems.values()]
    predict_args_for_em = [(em.mention_id, set(em.argument_index.keys())) for em in predict_ems.values()]
 
    pairs = itertools.product(gold_args_for_em, predict_args_for_em)
    matches = ((x[1].intersection(y[1]), x[0],y[0]) for (x,y) in pairs) 

    possible_matches = [(len(l),x,y) for (l,x,y) in matches if len(l) > 0]
    possible_matches.sort(reverse=True)
    
    seen_gold = set()
    seen_predict = set()
    result = []
    for l,gold_id,predict_id in possible_matches: 
        gold = gold_ems[gold_id]
        predict = predict_ems[predict_id]
        if (gold.event_type.lower() == predict.event_type.lower()) and (gold.trigger.offset == predict.trigger.offset) and (gold.trigger.length == predict.trigger.length) and (gold_id not in seen_gold) and (predict_id not in seen_predict): 
            result.append((gold_id, predict_id))
            seen_gold.add(gold_id)
            seen_predict.add(predict_id)
    return result

def compute_event_mention_map(gold_ere, predict_ere):
    i = 1

    pairs = get_arg_overlap_map(gold_ere.event_mentions, predict_ere.event_mentions) 
    gold_to_predict = dict(pairs)
    predict_to_gold = dict((v,k) for (k,v) in pairs)
    
    gold_mentions_to_new = {}
    predict_mentions_to_new = {}

    for predict_mention_id in predict_ere.event_mentions:
        predict_mention = predict_ere.event_mentions[predict_mention_id]
        mid = predict_mention.mention_id
        predict_mentions_to_new[mid] = "em-"+str(i)
        if mid in predict_to_gold:
            gold_mentions_to_new[predict_to_gold[mid]] = "em-"+str(i) 
        i+= 1   
            
    for gold_mention_id in gold_ere.event_mentions:
        gold_mention = gold_ere.event_mentions[gold_mention_id]
        mid = gold_mention.mention_id
        if mid not in gold_mentions_to_new: 
            gold_mentions_to_new[mid] = "em-"+str(i)
        i+=1         

    return gold_mentions_to_new, predict_mentions_to_new


def find_overlap(gold_mentions, predict_mentions):

    tp = 0
    fp = 0
    fn = 0
    for span in predict_mentions: 
        if span in gold_mentions: 
            tp += 1
        else: 
            fp += 1
    
    for span in gold_mentions:
        if not span in predict_mentions: 
            fn += 1
    print "TP", tp
    print "FP", fp
    print "FN", fn 
     
    print "Recall", tp / float(len(gold_mentions))
    print "Precision", tp/ float(len(predict_mentions))


def compute_entity_map(gold, predict, gold_map, predict_map):

    predict_entities_to_mentions = defaultdict(set)
    gold_entities_to_mentions = defaultdict(set)
    
    for entity_id in predict.entities: 
        entity = predict.entities[entity_id]
        predict_entities_to_mentions[entity_id] = set([predict_map[mention.mention_id] for mention in entity.mentions])
    
    for entity_id in gold.entities: 
        entity = gold.entities[entity_id]
        gold_entities_to_mentions[entity_id] = set([gold_map[mention.mention_id] for mention in entity.mentions if mention.mention_id in gold_map])
    

    gold_available = gold_entities_to_mentions.keys()
    predict_available = predict_entities_to_mentions.keys()

    # Inefficient, but functional
    pairs = itertools.product(gold_available, predict_available)
    matches = ((gold_entities_to_mentions[x].intersection(predict_entities_to_mentions[y]), x,y) for (x,y) in pairs) 
    possible_matches = [(len(l),x,y) for (l,x,y) in matches if len(l) > 0]
    possible_matches.sort(reverse=True)

    gold_seen = set()
    predict_seen = set()

    result_matches = []
    for (l, gold_entity, predict_entity) in possible_matches:
        if gold_entity not in gold_seen and predict_entity not in predict_seen:
            result_matches.append((predict_entity, gold_entity))
            gold_seen.add(gold_entity)
            predict_seen.add(predict_entity)


    predict_to_gold = dict(result_matches)

    i=0
    predict_to_new = dict()
    gold_to_new = dict()
    for predict_entity in predict.entities:
        predict_to_new[predict_entity] = "ent-"+str(i)
        if predict_entity in predict_to_gold:
            gold_to_new[predict_to_gold[predict_entity]] = "ent-"+str(i)
        i += 1
    for gold_entity in gold.entities: 
        if not gold_entity in gold_to_new: 
            gold_to_new[gold_entity] = "ent-"+str(i)
            i += 1

    return gold_to_new, predict_to_new
  

def compute_relation_map(gold, predict, gold_map, predict_map):

    predict_relations_to_mentions= defaultdict(set)
    gold_relations_to_mentions = defaultdict(set)
    
    for relation_id in predict.relations: 
        relation= predict.relations[relation_id]
        predict_relations_to_mentions[relation_id] = set([predict_map[mention.mention_id] for mention in relation.mentions])

    for relation_id in gold.relations: 
        relation= gold.relations[relation_id]
        gold_relations_to_mentions[relation_id] = set([gold_map[mention.mention_id] for mention in relation.mentions if mention.mention_id in gold_map])
    
    gold_available = gold_relations_to_mentions.keys()
    predict_available = predict_relations_to_mentions.keys()

    # Inefficient, but functional
    pairs = itertools.product(gold_available, predict_available)
    matches = ((gold_relations_to_mentions[x].intersection(predict_relations_to_mentions[y]), x,y) for (x,y) in pairs) 
    possible_matches = [(len(l),x,y) for (l,x,y) in matches if len(l) > 0]
    possible_matches.sort(reverse=True)


    gold_seen = set()
    predict_seen = set()

    result_matches = []
    for (l, gold_relation, predict_relation) in possible_matches:
        if gold_relation not in gold_seen and predict_relation not in predict_seen:
            result_matches.append((predict_relation, gold_relation))
            gold_seen.add(gold_relation)
            predict_seen.add(predict_relation)


    predict_to_gold = dict(result_matches)

    i=0
    predict_to_new = dict()
    gold_to_new = dict()
    for predict_relation in predict.relations:
        predict_to_new[predict_relation] = "r-"+str(i)
        if predict_relation in predict_to_gold:
            gold_to_new[predict_to_gold[predict_relation]] = "r-"+str(i)
        i += 1
    for gold_relation in gold.relations: 
        if not gold_relation in gold_to_new: 
            gold_to_new[gold_relation] = "r-"+str(i)
            i += 1

    return gold_to_new, predict_to_new


def compute_event_map(gold, predict, gold_map, predict_map):

    predict_events_to_mentions= defaultdict(set)
    gold_events_to_mentions = defaultdict(set)
    
    for event_id in predict.hoppers: 
        event = predict.hoppers[event_id]
        predict_events_to_mentions[event_id] = set([predict_map[mention.mention_id] for mention in event.mentions])
    for event_id in gold.hoppers: 
        event = gold.hoppers[event_id]
        gold_events_to_mentions[event_id] = set([gold_map[mention.mention_id] for mention in event.mentions])
    
    gold_available = gold_events_to_mentions.keys()
    predict_available = predict_events_to_mentions.keys()

    # Inefficient, but functional
    pairs = itertools.product(gold_available, predict_available)
    matches = ((gold_events_to_mentions[x].intersection(predict_events_to_mentions[y]), x,y) for (x,y) in pairs) 
    possible_matches = [(len(l),x,y) for (l,x,y) in matches if len(l) > 0]
    possible_matches.sort(reverse=True)


    gold_seen = set()
    predict_seen = set()

    result_matches = []
    for (l, gold_event, predict_event) in possible_matches:
        if gold_event not in gold_seen and predict_event not in predict_seen:
            result_matches.append((predict_event, gold_event))
            gold_seen.add(gold_event)
            predict_seen.add(predict_event)


    predict_to_gold = dict(result_matches)

    i=0
    predict_to_new = dict()
    gold_to_new = dict()
    for predict_event in predict.hoppers:
        predict_to_new[predict_event] = "h-"+str(i)
        if predict_event in predict_to_gold:
            gold_to_new[predict_to_gold[predict_event]] = "h-"+str(i)
        i += 1
    for gold_event in gold.hoppers: 
        if not gold_event in gold_to_new: 
            gold_to_new[gold_event] = "h-"+str(i)
            i += 1

    return gold_to_new, predict_to_new

def replace_names_in_txt(txt, mapping):
    for (k,v) in mapping.items(): 
        txt = txt.replace('"'+k+'"','"'+v+'"')
        txt = txt.replace("'"+k+"'","'"+v+"'")
    return txt

def rename(predict_ere_f, gold_ere_f, gold_best_f, outdir):
    predicted_ere_in = read_ere_xml(predict_ere_f)   
    gold_ere = read_ere_xml(gold_ere_f)   
    gold_best_txt = open(gold_best_f,'r').read()
    #find_overlap(get_entity_mention_dict(gold_ere), get_entity_mention_dict(predicted_ere_in))


    # map gold and predict mentions into a shared namespace
    gold_em_map, predict_em_map = compute_entity_mention_name_map(gold_ere, predicted_ere_in)

    # compute map from gold to predict entitites
    gold_ent_map, predict_ent_map  =  compute_entity_map(gold_ere, predicted_ere_in, gold_em_map, predict_em_map)

    # Update entitites and entity mentions
    predicted_ere_in.apply_ent_map(predict_ent_map, predict_em_map)
    gold_ere.apply_ent_map(gold_ent_map, gold_em_map)
    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_ent_map)
    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_em_map)

    # compute map from gold to predicted relations
    gold_relm_map, predict_relm_map = compute_relation_mention_name_map(gold_ere, predicted_ere_in)
    gold_rel_map, predict_rel_map = compute_relation_map(gold_ere, predicted_ere_in, gold_relm_map ,predict_relm_map)

    # Update relations and relation mentions 
    predicted_ere_in.apply_rel_map(predict_rel_map, predict_relm_map)
    gold_ere.apply_rel_map(gold_rel_map, gold_relm_map)
    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_rel_map)
    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_relm_map)
   
    # comptue map from gold to predicted events 
    gold_eventm_map, predict_eventm_map = compute_event_mention_map(gold_ere, predicted_ere_in)
    gold_event_map, predict_event_map = compute_event_map(gold_ere, predicted_ere_in, gold_eventm_map, predict_eventm_map)

    predicted_ere_in.apply_event_map(predict_event_map, predict_eventm_map)
    gold_ere.apply_event_map(gold_event_map, gold_eventm_map)

    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_event_map)
    gold_best_txt = replace_names_in_txt(gold_best_txt, gold_eventm_map)

    element = predicted_ere_in.to_element_tree()
    string = xml.dom.minidom.parseString(tostring(element)).toprettyxml() 

    source = str(gold_ere.sources.values()[0].doc_id)

    outf = codecs.open(os.path.join(outdir, source+".predicted.map.rich_ere.xml"),'w',"UTF-8")
    outf.write(string)
    outf.close()

    element = gold_ere.to_element_tree()
    string = xml.dom.minidom.parseString(tostring(element)).toprettyxml() 
    outf = codecs.open(os.path.join(outdir, source+".gold.map.rich_ere.xml"),'w',"UTF-8")
    outf.write(string)
    outf.close()

    outf = codecs.open(os.path.join(outdir, source+".map.best.xml"),'w',"UTF-8")
    outf.write(gold_best_txt.decode("UTF-8"))
    outf.close()
    
