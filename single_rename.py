import os.path
import sys

from rename_coref_chains import rename

predicted_f = sys.argv[1]
gold_f = sys.argv[2]
best_f = sys.argv[3]
out = sys.argv[4]

rename(predicted_f, gold_f, best_f, out)

