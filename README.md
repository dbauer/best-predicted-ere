# Scripts to create predicted ERE files for the TAC BeSt evaluation#

### Step 1: Converting APF to BeSt ERE annotations

Run the script rpi_apf_to_ere.py to convert an APF file, as produced by the RPI group, to a BeSt ERE file. 
Usage:
```
$ rpi_apf_to_ere.py <event_coreference_file.txt> <input_apf.xml>
```
Note that there is only one shared event coreference file that should be used for all input files.
The output ERE xml is written to stdout. Example:

```
$ mkdir predicted_ere
$ python rpi_apf_to_ere.py rpi_ere_results/coreference.txt  rpi_ere_results/LDC2016E27-source_ere_output/010aaf594ae6ef20eb28e3ee26038375.cmp.txt.apf.xml > predicted_ere/010aaf594ae6ef20eb28e3ee26038375.predicted.rich_ere.xml
```

To run this as a batch job for all apf files: 
```
$ for f in rpi_ere_results/*.apf.xml; do python rpi_apf_to_ere.py rpi_ere_results/coreference.txt $f > $f.predicted.rich_ere.xml; done
# Move result files to new directory and rename
$ mkdir predicted_ere
$ mv rpi_ere_results/*.predicted.rich_ere.xml predicted_ere
$ cd predicted_ere
$ for f in *.predicted.rich_ere.xml; do mv $f `echo $f | sed s/\.cmp\.txt.apf\.xml//`; done
```
### Step 2: Matching predicted entities/relations/events to gold
In this step, we try to find a match for the predicted entities, relations, and events to the gold annotation. We then map the IDs used in the predicted and in the gold files into a shared name space. We write out a new predicted rich ere xml file, as well as a modified best file.