import os.path
import sys

from rename_coref_chains import rename


predicted_path = sys.argv[1]
gold_path = sys.argv[2]
best_path = sys.argv[3]
out_path = sys.argv[4]

predict_files = os.listdir(sys.argv[1])

docs = [x.replace(".predicted.rich_ere.xml","") for x in predict_files if x.endswith("xml")]

for doc in docs:
    print doc
    predict_f = os.path.join(predicted_path, doc+".predicted.rich_ere.xml")
    gold_f = os.path.join(gold_path, doc+".rich_ere.xml")
    best_f = os.path.join(best_path, doc+".best.xml")
    rename(predict_f, gold_f, best_f, out_path)

