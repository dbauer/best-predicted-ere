#!/usr/bin/env python
"""
This module contains classes to represent DEFT ERE annotations and methods
to read ERE XML annotations into an OOP representation.
"""
import xml.etree.ElementTree as ET
from xml.etree.ElementTree import tostring
import xml.dom.minidom
from collections import defaultdict
from deft_ere import EREAnnotations, DocumentSource, Entity, EntityMention, Relation, RelationMention, Hopper, EventMention, Argument, Trigger, NomHead
import sys
import re
import os

__author__ = 'Daniel Bauer'
__email__ = 'bauer@cs.columbia.edu'
__date__ = 'August 17 2016'

##############################################################################
# The following section contains a top-level class EREAnnotations that stores 
# ERE annotationsand functions to populate this object from an XML source. 


relation_mapping = {"ORG-AFF":"orgaffiliation",
                    "PART-WHOLE":"partwhole",
                    "GEN-AFF":"generalaffiliation",
                    "PER-SOC":"personalsocial",
                    "PHYS":"physical",
                    "ART":"artifact"
                   }



class ApfAnnotations(object): 
    """
    Represent a set of EREAnnotations, possibly over multiple documents 
    """

    def __init__(self):
        self.sources = {}
        self.entities = {}
        self.entity_mentions = {}
        self.fillers = {}
        self.relations = {}
        self.relation_mentions = {}
        self.events = {}
        self.event_mentions = {}
        self.coreference_id = {}

    def to_element_tree(self):
        if len(self.sources)>1: 
            raise NotImplementedError("Don't know how to XML-ify multiple sources.")
        
        docsrc = self.sources.values()[0]
       
        element = ET.Element('deft_ere', {'kit_id':docsrc.kit_id, 'doc_id':docsrc.doc_id, 'source_type':docsrc.source_type})

        entity_element = ET.Element('entities') 
        for entity in self.entities.values():
            entity_element.append(entity.to_element_tree())
        element.append(entity_element)

        relation_element = ET.Element('relations')
        for relation in self.relations.values():
            relation_element.append(relation.to_element_tree())
        element.append(relation_element)
        
        hopper_element = ET.Element('hoppers')
        for event in self.events.values():
            hopper_element .append(event.to_element_tree())
        element.append(hopper_element)

        return element
    

    def integrate_entity(self, entity_et, source):
        """
        Add the entity described in this etree object into the annotation data structure.
        Note that if an entity element uses an existing id, its entity mentions will just be
        added to the original entity object. 
        """
        entity = Entity(entity_et.get('ID'), entity_et.get('TYPE'), source_doc = source)
        if not self.entities.get(entity.entity_id):
            self.entities[entity.entity_id] = entity
        else: 
            entity = self.entities.get(entity.entity_id) 
        
        for child_et in entity_et:  # add each mention id
            if child_et.tag == 'entity_mention':

                start = 0
                end = 0
                length = 0
                
                nom_head = None
                for mentionchild_et in child_et:
                    if mentionchild_et.tag == 'extent':
                        charseq_et = mentionchild_et.find('charseq')
                        start = int(charseq_et.get("START"))
                        end = int(charseq_et.get("END"))
                        length = end - start + 1
                        text = charseq_et.text
                    elif mentionchild_et.tag == 'head':
                        head_charseq_et = mentionchild_et.find('charseq')
                        head_start = int(head_charseq_et.get("START"))
                        head_end = int(head_charseq_et.get("END"))
                        head_length = head_end - head_start + 1
                        head_text = head_charseq_et.text
                        nom_head = NomHead(head_start, head_length, head_text, source) 
                        
                    else: 
                        raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))
                
                mention = EntityMention(child_et.get('ID'), child_et.get('TYPE'), source, start, length, entity)
                mention.nom_head = nom_head
                mention.mention_text = text

                if not mention.mention_id in self.entity_mentions:
                    self.entity_mentions[mention.mention_id] = mention
                    entity.mentions.append(mention)
                else: 
                    mention = self.entity_mentions[mention.mention_id]

                self.coreference_id[mention.mention_id] = child_et.get('COREFERENCE_ID')
           
                       
    def integrate_relation(self, relation_et, source): 
        """
        Add the relations described in this etree object into the annotation data structure.
        """

        reltype = relation_et.get('TYPE')
        if not reltype in relation_mapping:
            raise RuntimeError('Unknown relation type '+reltype)
        
        relation = Relation(relation_et.get('ID'), relation_mapping[reltype], '') 
    
        

        if not relation.relation_id in self.relations: 
            self.relations[relation.relation_id] = relation
        else: 
            relation = self.relations[relation.relation_id] 
        
        for child_et in relation_et: 


            if child_et.tag == 'relation_mention':

                mention = RelationMention(child_et.get('ID'), None)
                if not mention.mention_id in self.relation_mentions:
                    self.relation_mentions[mention.mention_id] = mention
                    relation.mentions.append(mention)
                    mention.relation = relation
                else: 
                    mention = self.relation_mentions[mention.mention_id]
 

                # Add the arguments
                for mentionchild_et in child_et:
                    if mentionchild_et.tag == "ldc_extent":
                        pass
                    elif mentionchild_et.tag == "relation_mention_argument":
                        entity_mention = self.entity_mentions[mentionchild_et.get('REFID')]
                        entity = entity_mention.entity
                        role = mentionchild_et.get('ROLE')
                        arg = Argument(entity, entity_mention, role)
                        arg.text = child_et.text
                        charseq = mentionchild_et.find('extent').find('charseq')
                        start = charseq.get('START')
                        end = charseq.get('END')

                        arg.text = charseq.text
                        if role == "Arg-1":
                            mention.rel_arg1 = arg
                        elif role == "Arg-2":
                            mention.rel_arg2 = arg
                        else:
                            raise AnnotationFormatError('Unexpected argument type for relation mention %s' % mention.mention_id)
            elif child_et.tag == 'relation_argument':
                pass
            else: 
                raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))

    def integrate_event(self, event_et, source, coref): 
        """
        Add the events described in this etree object into the annotation data structure.
        """
 

        
        event_id = event_et.get('ID')
        if event_id in coref:  # get representative for this event coreference cluster
            event_id = coref[event_id]

        event = Hopper(event_id)

        event_type = event_et.get('TYPE')
        event_subtype = event_et.get('SUBTYPE')
        event_modality = event_et.get('MODALITY')

        if not event.event_id in self.events: 
            self.events[event.event_id] = event 
        else: 
            event = self.events[event.event_id] 
        
        for child_et in event_et: 

            if child_et.tag == 'event_mention':

                mention = EventMention(child_et.get('ID'), event_type, event_subtype, None)
                if not mention.mention_id in self.event_mentions:
                    self.event_mentions[mention.mention_id] = mention
                    event.mentions.append(mention)
                    mention.hopper = event 
                else: 
                    mention = self.event_mentions[mention.mention_id]

                # Add the arguments
                for mentionchild_et in child_et:
                    if mentionchild_et.tag == "extent":
                        pass
                    elif mentionchild_et.tag == "event_mention_argument":
                        entity_mention = self.entity_mentions[mentionchild_et.get('REFID')]
                        entity = entity_mention.entity
                        role = mentionchild_et.get('ROLE')
                        arg = Argument(entity, entity_mention, role)

                        charseq = mentionchild_et.find('extent').find('charseq')
                        start = int(charseq.get('START'))
                        end = int(charseq.get('END'))
                        length = start - end + 1
                        arg.text = charseq.text
                        mention.arguments[arg.role] = arg
                        mention.argument_index[arg.entity.entity_id].append(arg)
                    elif mentionchild_et.tag == "anchor":
                        charseq = mentionchild_et.find("charseq")
                        start = int(charseq.get("START"))
                        end = int(charseq.get("END"))
                        length = end - start + 1
                        trigger = Trigger(source, start, length) 
                        trigger.text = charseq.text
                        mention.trigger = trigger
                    else:
                        raise AnnotationFormatError('Unexpected argument type for event mention %s' % mention.mention_id)
            else: 
                raise AnnotationFormatError('unexpected XML element {0}, child of {1}.'.format(tostring(child_et), tostring(mention_et)))
      
    def make_entity_corefs(self): 
        
        new_entities = {}

        cluster_to_mentions = defaultdict(list)

        prototype_entity = {}
        rename_entities = {}

        for em_id in self.entity_mentions:
            em = self.entity_mentions[em_id]
            old_entity_id = em.entity.entity_id
            cluster = self.coreference_id[em_id]
            cluster_to_mentions[cluster].append(em)
            if cluster in prototype_entity: 
                old_entity = prototype_entity[cluster]
                em.entity = old_entity
                if em.entity.entity_type!=old_entity.entity_type:    
                    raise AnnotationFormatError('Incompatible entity types in coref chain {0}: {1} is {2} and {3} is {4}.'.format(cluster, old_entity.entity_id, old_entity.entity_type, em.entity.entity_id, em.entity.entity_type))
            else: 
                prototype_entity[cluster] = em.entity
            rename_entities[old_entity_id] = prototype_entity[cluster]  

 
        for cluster in cluster_to_mentions: 
            entity = prototype_entity[cluster]
            entity.mentions = cluster_to_mentions[cluster]
            new_entities[entity.entity_id] = entity
        
        self.entities = new_entities      

        for relation_men in self.relation_mentions:
            relation_mention = self.relation_mentions[relation_men]
            arg1 = relation_mention.rel_arg1
            arg1.entity = rename_entities[arg1.entity.entity_id].entity_id             
            arg2 = relation_mention.rel_arg2
            arg2.entity = rename_entities[arg2.entity.entity_id].entity_id   

        for event_men in self.event_mentions: 
            event_mention = self.event_mentions[event_men]
            event_mention.argument_index = defaultdict(list)
            for role in event_mention.arguments: # role => Argument objects
                arg = event_mention.arguments[role]
                arg.entity = rename_entities[arg.entity.entity_id].entity_id
                event_mention.argument_index[arg.entity].append(arg)
 
    def integrate_etree(self, root, coref):
        """
        Add the content of an e-tree object into the annotation data structure.
        """
        # Create a new source object
        source = DocumentSource('', os.path.basename(root.get('DOCID')).replace(".cmp.txt",""))
        if not source.doc_id in self.sources: 
            self.sources[source.doc_id] = source
        else: 
            source = self.sources[source.doc_id] 
        
        # Now process the children
        for child in root: 
            if child.tag == 'entity':
                self.integrate_entity(child, source)
            elif child.tag == 'relation':
                self.integrate_relation(child, source)
                pass
            elif child.tag == 'event':
                self.integrate_event(child, source, coref)
                pass
            else: 
                raise AnnotationFormatError('unexpected XML element {0} at top level.'.format(tostring(child)))

        self.make_entity_corefs()


def read_coref_info(source_file):

    result = {}
    for line in source_file: 
        if line.startswith("@Coreference"):
            line = line.split()
            coref_chain = line[2]
            coref_events = coref_chain.split(",")
            coref_events = [re.sub("-1$","",x) for x in coref_events]
            representative = coref_events[0]
            for coref_event in coref_events:
                result[coref_event] = representative
    return result
                 

def read_apf_xml(coref, *sources):
    """
    Read in one or more XML sources and return an EREAnnotation object. 
    """  

    annotations = ApfAnnotations(); # The new annotations object

    for source_f in sources: 
        et = ET.parse(source_f)
        annotations.integrate_etree(et.getroot().find('document'), coref)

    return annotations

if __name__ == "__main__":
    with open(sys.argv[1],'r') as coref_file:
       coref = read_coref_info(coref_file)
    annos = read_apf_xml(coref, sys.argv[2])

    element = annos.to_element_tree()
    string = xml.dom.minidom.parseString(tostring(element)).toprettyxml() 
    print(string.encode("UTF-8"))
    
